package com.example.covid_app_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDateTime;


public class SubmissionActivity extends AppCompatActivity {

    EditText dateText, tempText, weightText;

    private static String port = "1433";
    private static String url = "theprocrastinators.database.windows.net";
    private static String classLoc = "net.sourceforge.jtds.jdbc.Driver";
    private static String database = "covid_application";
    private static String username = "ljmu";
    private static String Databasepassword = "EpicGamer69";
    private static String connecturl = "jdbc:jtds:sqlserver://"+url+":"+port+"/"+database;
    private Connection connection = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission);

        Spinner stressSpinner = findViewById(R.id.submission_stress_level);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(SubmissionActivity.this,android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.spinnerStressValues));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        stressSpinner.setAdapter(myAdapter);

        Button submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(v -> {
            try {
                checkEmpty();
            } catch (ParseException E) {
                Log.e("Parse exception", E.getMessage());
            }
        });
    }

    public void checkEmpty() throws ParseException {

        //getting all the user inputs and storing them into variables
        tempText = findViewById(R.id.submission_temp);
        double temp;
        if (tempText.getText().toString().isEmpty())
        {
             temp = 0;
        }
        else
        temp = Double.parseDouble(tempText.getText().toString());

        Spinner stresslvlText = findViewById(R.id.submission_stress_level);
        int stresslvl = Integer.parseInt(stresslvlText.getSelectedItem().toString());

        weightText = findViewById(R.id.submission_weight);
        double weight;
        if ((weightText.getText().toString().isEmpty()))
        {
            weight = 0;
        }
        else
        weight = Integer.parseInt(weightText.getText().toString());

        RadioButton contCoughBtn = findViewById(R.id.submission_coughBtn);
        boolean contCough = contCoughBtn.isChecked();

        RadioButton fluLikeBtn = findViewById(R.id.submission_fluBtn);
        boolean fluLike = fluLikeBtn.isChecked();

        RadioButton lossTasteSmellBtn = findViewById(R.id.submission_tastesmellBtn);
        boolean lossTasteSmell = lossTasteSmellBtn.isChecked();

        RadioButton hypertensionBtn = findViewById(R.id.submission_hypertension);
        boolean hypertension = hypertensionBtn.isChecked();

        RadioButton lossAppetiteBtn = findViewById(R.id.submission_loss_appetiteBtn);
        boolean lossAppetite = lossAppetiteBtn.isChecked();

        RadioButton shortnessBreathBtn = findViewById(R.id.submission_short_breath);
        boolean shortnessBreath = shortnessBreathBtn.isChecked();

        LocalDateTime dateTime = LocalDateTime.now();

        if (!validation(temp, stresslvl, weight)) {
            Record newRecord = new Record(MainActivity.currentPatientID, dateTime, temp, stresslvl, weight, contCough, fluLike, lossTasteSmell, hypertension, shortnessBreath, lossAppetite);
            SendSymptomData(newRecord);
        }
    }

    public boolean validation(double temp, int stresslvl, double weight) {
        boolean flag = false;
        if (temp==0)
        {
            flag = true;
            tempText.setError("Temperature required");
        }
        else if (stresslvl==0)
        {
            flag = true;
            Toast.makeText(SubmissionActivity.this, "Stress Level not Selected",
                    Toast.LENGTH_SHORT).show();

        }
        else if (weight==0)
        {
            flag = true;
            weightText.setError("Weight required");
        }
        return flag;
    }

    public void SendSymptomData(Record record){
        connect();
        if(connection != null){
            try{
                PreparedStatement registerRecord = connection.prepareStatement("EXEC dbo.InsertNewRecord @PatientID ='"+record.getPatientID()+"', @DateTimeOfRecord ='" +record.getCurrentDateTime()+"', @Temp = '"+record.getTemp()+"', @StressLevel = '"+record.getStressLevels()+"', @Weight = '"+record.getWeight()+"', @ContCough = '"+record.isContCought()+"', @FluLike = '"+record.isFluLike()+"', @LossOfTaste = '"+record.isLossOfTaste()+"', @Hypertension = '"+record.isHypertension()+"', @ShortOfBreath = '"+record.isShortOfBreath()+"', @LossOfApatite = '"+record.isLossOfAppetite()+"';");
                registerRecord.executeUpdate();

                connection.close();
                registerRecord.close();
                Toast.makeText(SubmissionActivity.this, "Symptom Report has been submitted",
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);

            } catch(SQLException E){
                Toast.makeText(SubmissionActivity.this, E.getMessage(),
                        Toast.LENGTH_LONG).show();
            }}
        else{
            TextView text = findViewById(R.id.page_name_register);
            text.setText("connection null");
        }
    }

    public Connection connect(){

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try{
            //points to mssql driver
            Class.forName(classLoc);

            //create connection to mssql database
            connection = DriverManager.getConnection(connecturl, username, Databasepassword);
        }catch (SQLException E){
            Log.e("SQL error", E.getMessage());
        }
        catch (ClassNotFoundException E){
            Log.e("Class error", E.getMessage());
        }
        return connection;
    }
}