package com.example.covid_app_mobile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;

public class RegisterActivity extends AppCompatActivity {

    private Connection connection = null;

    EditText emailText, firstNameText, surnameText, additionalConditionsText, addressLine1Text, addressLine2Text, cityText, countyText, postcodeText, passwordText, passwordConfirmText, dobText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, PackageManager.PERMISSION_GRANTED);

        Spinner hospitalSpinner =  findViewById(R.id.register_hospitalisations);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(RegisterActivity.this,android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.spinnerValues));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner genderSpinner = findViewById(R.id.register_gender);
        ArrayAdapter<String> genderAdapter =  new ArrayAdapter<>(RegisterActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.Genders));
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderAdapter);

        hospitalSpinner.setAdapter(myAdapter);

        Spinner noHouseMembers = findViewById(R.id.register_no_house_members);
        noHouseMembers.setAdapter(myAdapter);

        Button registerBtn = findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(v -> {
            try {
                connection = connect();
                checkEmpty();
            } catch (ParseException E) {
                Log.e("Parse exception", E.getMessage());
            }
        });
    }


    public void checkEmpty() throws ParseException {

        //getting all the user inputs and storing them into variables
        emailText = findViewById(R.id.register_email);
        String email = emailText.getText().toString();
        firstNameText = findViewById(R.id.register_firstname);
        String firstName = firstNameText.getText().toString();
        surnameText = findViewById(R.id.register_surname);
        String surname = surnameText.getText().toString();


        java.sql.Date sqlDate = null;
        dobText = findViewById(R.id.register_dob);
        String dob = dobText.getText().toString();
        //SimpleDateFormat newFormatter = new SimpleDateFormat("dd-mm-yyyy");
        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
        if(dob.isEmpty()){
            dobText.setError("Date of birth required");
        }
        else if(!dob.matches("\\d{4}-\\d{2}-\\d{2}")){
            dobText.setError("Please format the date as yyyy-mm-dd");
        }
        else{
            sqlDate = java.sql.Date.valueOf(dob);
        }

        Spinner hospitalisationsSpinner = findViewById(R.id.register_hospitalisations);
        int hospitalisations = Integer.parseInt(hospitalisationsSpinner.getSelectedItem().toString());
        Spinner houseMembersSpinner = findViewById(R.id.register_no_house_members);
        int houseMembers = Integer.parseInt(houseMembersSpinner.getSelectedItem().toString());
        Spinner genderSpinner = findViewById(R.id.register_gender);
        String gender = genderSpinner.getSelectedItem().toString();
        RadioButton diabetesBtn = findViewById(R.id.register_diabetesBtn);
        boolean diabetes = diabetesBtn.isChecked();
        RadioButton publicTransportBtn = findViewById(R.id.register_transportBtn);
        boolean publicTransport = publicTransportBtn.isChecked();
        RadioButton pregnantBtn = findViewById(R.id.register_pregnantBtn);
        boolean pregnant = pregnantBtn.isChecked();
        RadioButton smokerBtn = findViewById(R.id.register_smokerBtn);
        boolean smoker = smokerBtn.isChecked();
        additionalConditionsText = findViewById(R.id.register_additionalConditions);
        String additionalConditions = additionalConditionsText.getText().toString();
        addressLine1Text = findViewById(R.id.register_addressLine1);
        String addressLine1 = addressLine1Text.getText().toString();
        addressLine2Text = findViewById(R.id.register_addressLine2);
        String addressLine2 = addressLine2Text.getText().toString();
        cityText = findViewById(R.id.register_city);
        String city = cityText.getText().toString();
        countyText = findViewById(R.id.register_county);
        String county = countyText.getText().toString();
        postcodeText = findViewById(R.id.register_postcode);
        String postcode = postcodeText.getText().toString();
        passwordText = findViewById(R.id.register_password);
        String password = passwordText.getText().toString();
        passwordConfirmText = findViewById(R.id.register_confirm_password);
        String passwordConfirm = passwordConfirmText.getText().toString();

        if(!validation(email, firstName, surname, addressLine1, city, county, postcode, password, passwordConfirm)){
        Patient patient = new Patient(firstName,surname,email,sqlDate,gender,smoker,pregnant,additionalConditions,publicTransport,houseMembers,hospitalisations,diabetes,addressLine1,addressLine2,city,county,postcode,password);

        if(!checkEmail(patient)){
            registerData(patient);
        }else{
            emailText.setError("Email already exists");
        }
        }
    }

    public boolean validation(String email, String firstname, String surname, String addressLine1, String city, String county, String postcode,String password,String passwordConfirm){
        boolean flag = false;
        String regex = "^[aA-zZ]+";
        if(email.isEmpty()){
            emailText.setError("Email is required");
            flag = true;
        }
        else if (!email.matches("(?:[aA-zZ0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[aA-zZ0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[aA-zZ0-9](?:[aA-zZ0-9-]*[aA-zZ0-9])?\\.)+[aA-zZ0-9](?:[aA-zZ0-9-]*[aA-zZ0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[aA-zZ0-9-]*[aA-zZ0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")){
            emailText.setError("Please enter a valid email address");
            flag = true;
        }
        else if(firstname.isEmpty()|| !firstname.matches(regex)) {
            firstNameText.setError("First name is required and must be valid");
            flag = true;
        }else if(surname.isEmpty()|| !surname.matches(regex)){
            surnameText.setError("Surname is required and must be valid");
            flag = true;
        }else if(addressLine1.isEmpty()){
            addressLine1Text.setError("This is required");
            flag = true;
        }else if(city.isEmpty()|| !city.matches(regex)){
            cityText.setError("City required and must be valid");
            flag = true;
        }else if(county.isEmpty()|| !county.matches(regex)){
            countyText.setError("County required and must be valid");
            flag = true;
        }else if(postcode.isEmpty()){
            postcodeText.setError("Postcode required");
            flag = true;
        }else if(password.isEmpty()){
            passwordText.setError("Password required");
            flag = true;
        }
        else if(passwordConfirm.isEmpty()){
            passwordConfirmText.setError("Password confirmation required");
            flag = true;
        }
        else if(!password.equals(passwordConfirm)){
            passwordConfirmText.setError("Passwords must be the same");
        }
        return flag;
    }

    public void registerData(Patient patient){

        if(connection != null){
        try{

            String userID = null;
            String addressID = null;
            int patientID = 0;

            PreparedStatement createUser = connection.prepareStatement("EXEC dbo.InsertNewUsers @Email='"+patient.getEmail()+"',@Passwd='"+patient.getPassword()+"',@FirstName='"+patient.getFirstName()+ "',@Surname='"+patient.getSurname()+"';");
            createUser.executeUpdate();

            String userIdQuery = "SELECT UserID FROM Users WHERE Email = '"+patient.getEmail()+"';";
            String userAddressIdQuery = "SELECT AddressID FROM Addresses WHERE Postcode= '"+patient.getPostcode()+"' AND AddressLine1 = '"+patient.getAddressLine1()+"' AND AddressLine2 = '"+patient.getAddressLine2()+"';";

            PreparedStatement getAddressID = connection.prepareStatement(userAddressIdQuery);
            PreparedStatement getUserID = connection.prepareStatement(userIdQuery);

            ResultSet resultUserID= getUserID.executeQuery();
            ResultSet resultAddressID = getAddressID.executeQuery();

            while(resultUserID.next()){
                userID = resultUserID.getString(1);
            }
            while(resultAddressID.next()){
                addressID = resultAddressID.getString(1);
            }

            if(addressID == null) {

                PreparedStatement createAddress = connection.prepareStatement("EXEC dbo.InsertNewAddress @AddressLine1='" + patient.getAddressLine1() + "',@AddressLine2='" + patient.getAddressLine2() + "',@City ='" + patient.getCity() + "',@County ='" + patient.getCounty() + "',@Postcode ='" + patient.getPostcode() + "';", Statement.RETURN_GENERATED_KEYS);
                createAddress.executeUpdate();
                resultAddressID = getAddressID.executeQuery();
                while(resultAddressID.next()){
                    addressID = resultAddressID.getString(1);
                }
            }

            PreparedStatement registerPatient = connection.prepareStatement("EXEC dbo.InsertNewPatient @DoB ='"+patient.getDob().toString()+"',@Gender ='"+patient.getGender()+"',@NoOfPplInHouse ='"+patient.getNumberPeopleHousehold()+"',@UseOfPubTrans ='"+patient.isUsesPublicTransport()+"',@NoOfHos ='"+patient.getNumberHospitalisations()+"',@Diabetes ='"+patient.isDiabetes()+"',@UserID ="+userID+",@AddressID ="+addressID+",@Pregnant ='"+patient.isPregnant()+"',@Smoker ='"+patient.isSmoker()+"',@AdditionalConditions ='"+patient.getAdditionalConditions()+"';");
            registerPatient.executeUpdate();

            String getUserIdQuery = "SELECT patientID FROM Patients WHERE userID = '"+userID+"';";
            PreparedStatement getPatientID = connection.prepareStatement(getUserIdQuery);
            ResultSet resultPatientID = getPatientID.executeQuery();
            while(resultPatientID.next()){
                patientID = resultPatientID.getInt(1);
            }

            MainActivity.currentPatientID = patientID;
            connection.close();
            resultUserID.close();
            resultAddressID.close();
            resultPatientID.close();

            MainActivity.userID = userID;

            Intent intent = new Intent(this, SubmissionActivity.class);
            startActivity(intent);


        } catch(SQLException E){
            Log.e("SQL error", E.getMessage());
            E.printStackTrace();
        }}
        else{
            Toast toast = Toast.makeText(RegisterActivity.this, "Connection failure", Toast.LENGTH_LONG);
            toast.show();

        }

    }

    public boolean checkEmail(Patient patient){

        String emailQuery= "SELECT Email FROM Users WHERE Email = '"+patient.getEmail()+"';";
        String email ="";
        try {
            PreparedStatement emailstm = connection.prepareStatement(emailQuery);
            ResultSet result = emailstm.executeQuery();
            while(result.next()){
                email = result.getString("Email");
            }
            if(email.isEmpty()){
                return false;
            }

        }catch (SQLException E){
            Log.e("SQL error", E.getMessage());
        }
        return true;
    }


    public Connection connect(){

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try{
            //points to mssql driver
             Class.forName(MainActivity.classLoc);

            //create connection to mssql database
            connection = DriverManager.getConnection(MainActivity.connecturl, MainActivity.username, MainActivity.Databasepassword);
         }catch (SQLException E){
            Log.e("SQL error", E.getMessage());
        }
        catch (ClassNotFoundException E){
            Log.e("Class error", E.getMessage());
        }

        return connection;
    }



    }
