package com.example.covid_app_mobile;

import java.time.LocalDateTime;

public class Record {
        private int patientID;
        private LocalDateTime currentDateTime;
        private Double temp;
        private Integer stressLevels;
        private Double weight;
        private boolean contCought;
        private boolean fluLike;
        private boolean lossOfTaste;
        private boolean hypertension;
        private boolean shortOfBreath;
        private boolean lossOfAppetite;

        public Record(int patientID, LocalDateTime currentDateTime, Double temp, Integer stressLevels, Double weight, boolean contCought, boolean fluLike, boolean lossOfTaste, boolean hypertension, boolean shortOfBreath, boolean lossOfAppetite) {
            this.patientID = patientID;
            this.currentDateTime = currentDateTime;
            this.temp = temp;
            this.stressLevels = stressLevels;
            this.weight = weight;
            this.contCought = contCought;
            this.fluLike = fluLike;
            this.lossOfTaste = lossOfTaste;
            this.hypertension = hypertension;
            this.shortOfBreath = shortOfBreath;
            this.lossOfAppetite = lossOfAppetite;
        }

        public int getPatientID() {
            return patientID;
        }

        public void setPatientID(int patientID) {
            this.patientID = patientID;
        }

        public LocalDateTime getCurrentDateTime() {
            return currentDateTime;
        }

        public void setCurrentDateTime(LocalDateTime currentDateTime) {
            this.currentDateTime = currentDateTime;
        }

        public Double getTemp() {
            return temp;
        }

        public void setTemp(Double temp) {
            this.temp = temp;
        }

        public Integer getStressLevels() {
            return stressLevels;
        }

        public void setStressLevels(Integer stressLevels) {
            this.stressLevels = stressLevels;
        }

        public Double getWeight() {
            return weight;
        }

        public void setWeight(Double weight) {
            this.weight = weight;
        }

        public boolean isContCought() {
            return contCought;
        }

        public void setContCought(boolean contCought) {
            this.contCought = contCought;
        }

        public boolean isFluLike() {
            return fluLike;
        }

        public void setFluLike(boolean fluLike) {
            this.fluLike = fluLike;
        }

        public boolean isLossOfTaste() {
            return lossOfTaste;
        }

        public void setLossOfTaste(boolean lossOfTaste) {
            this.lossOfTaste = lossOfTaste;
        }

        public boolean isHypertension() {
            return hypertension;
        }

        public void setHypertension(boolean hypertension) {
            this.hypertension = hypertension;
        }

        public boolean isShortOfBreath() {
            return shortOfBreath;
        }

        public void setShortOfBreath(boolean shortOfBreath) {
            this.shortOfBreath = shortOfBreath;
        }

        public boolean isLossOfAppetite() {
            return lossOfAppetite;
        }

        public void setLossOfAppetite(boolean lossOfAppetite) {
            this.lossOfAppetite = lossOfAppetite;
        }
    }

