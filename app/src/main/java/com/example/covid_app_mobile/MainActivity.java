package com.example.covid_app_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MainActivity extends AppCompatActivity  {

    static int currentPatientID = 0;
    private static String port = "1433";
    private static String url = "theprocrastinators.database.windows.net";
    public static String classLoc = "net.sourceforge.jtds.jdbc.Driver";
    private static String database = "covid_application";
    public static String username = "ljmu";
    public static String Databasepassword = "EpicGamer69";
    public static String connecturl = "jdbc:jtds:sqlserver://"+url+":"+port+"/"+database;

    public static Connection connection = null;

    EditText emailText, passwordText;

    static String userID = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button registerBtn = findViewById(R.id.login_registerBtn);
        registerBtn.setOnClickListener(v -> openRegister());

        Button loginBtn = findViewById(R.id.loginButton);
        loginBtn.setOnClickListener(v -> checkLogin());

    }

    public void openRegister(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void checkLogin(){

    emailText = findViewById(R.id.loginEmailAddress);
    String email = emailText.getText().toString();
    passwordText = findViewById(R.id.loginPassword);
    String password = passwordText.getText().toString();
    int patientID = 0;

    if(email.isEmpty()){
        emailText.setError("Email address required");
    }else if(password.isEmpty()){
        passwordText.setError("Password required");
    }   else{

        if(connect() != null) {
            String checkSQL = "SELECT UserID, Passwd FROM Users WHERE Email ='" + email + "';";

            try {
                PreparedStatement getUserID = connection.prepareStatement(checkSQL);
                ResultSet resultUserID = getUserID.executeQuery();
                String passwd ="";

                while(resultUserID.next()){
                    userID = resultUserID.getString(1);
                    passwd = resultUserID.getString(2);
                }
                if(userID == null || !passwd.equals(password)){
                    emailText.setError("Email address or password incorrect");
                } else if(userID != null && passwd.equals(password)){

                    String getUserIdQuery = "SELECT PatientID FROM Patients WHERE UserID = "+userID+";";
                    PreparedStatement getPatientID = connection.prepareStatement(getUserIdQuery);
                    ResultSet resultPatientID = getPatientID.executeQuery();
                    while(resultPatientID.next()){
                        patientID = resultPatientID.getInt(1);
                    }
                    currentPatientID = patientID;

                    Intent intent = new Intent(this, SubmissionActivity.class);
                    startActivity(intent);
                }
            }catch(SQLException E){
                Log.e("SQL error", E.getMessage());
            }
        }
    }
    }

    public Connection connect(){

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try{
            //points to mssql driver
            Class.forName(classLoc);

            //create connection to mssql database
            connection = DriverManager.getConnection(connecturl, username, Databasepassword);
        }catch (SQLException E){
            Log.e("SQL error", E.getMessage());
        }
        catch (ClassNotFoundException E){
            Log.e("Class error", E.getMessage());
        }

        return connection;
    }

}