package com.example.covid_app_mobile;

import java.util.Date;

public class Patient {
    private String firstName;
    private String surname;
    private String email;
    private Date dob;
    private String gender;
    private boolean smoker;
    private boolean pregnant;
    private String additionalConditions;
    private boolean usesPublicTransport;
    private int numberPeopleHousehold;
    private int numberHospitalisations;
    private boolean diabetes;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String county;
    private String postcode;
    private String password;

    public Patient(String firstName, String surname, String email, Date dob, String gender, boolean smoker, boolean pregnant, String additionalConditions, boolean usesPublicTransport, int numberPeopleHousehold, int numberHospitalisations, boolean diabetes, String addressLine1, String addressLine2, String city, String county, String postcode, String password) {
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.dob = dob;
        this.gender = gender;
        this.smoker = smoker;
        this.pregnant = pregnant;
        this.additionalConditions = additionalConditions;
        this.usesPublicTransport = usesPublicTransport;
        this.numberPeopleHousehold = numberPeopleHousehold;
        this.numberHospitalisations = numberHospitalisations;
        this.diabetes = diabetes;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.county = county;
        this.postcode = postcode;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isSmoker() {
        return smoker;
    }

    public void setSmoker(boolean smoker) {
        this.smoker = smoker;
    }

    public boolean isPregnant() {
        return pregnant;
    }

    public void setPregnant(boolean pregnant) {
        this.pregnant = pregnant;
    }

    public String getAdditionalConditions() {
        return additionalConditions;
    }

    public void setAdditionalConditions(String additionalConditions) {
        this.additionalConditions = additionalConditions;
    }

    public boolean isUsesPublicTransport() {
        return usesPublicTransport;
    }

    public void setUsesPublicTransport(boolean usesPublicTransport) {
        this.usesPublicTransport = usesPublicTransport;
    }

    public int getNumberPeopleHousehold() {
        return numberPeopleHousehold;
    }

    public void setNumberPeopleHousehold(int numberPeopleHousehold) {
        this.numberPeopleHousehold = numberPeopleHousehold;
    }

    public int getNumberHospitalisations() {
        return numberHospitalisations;
    }

    public void setNumberHospitalisations(int numberHospitalisations) {
        this.numberHospitalisations = numberHospitalisations;
    }

    public boolean isDiabetes() {
        return diabetes;
    }

    public void setDiabetes(boolean diabetes) {
        this.diabetes = diabetes;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
